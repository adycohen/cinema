import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'removeNonEnglishLetters'
})
export class RemoveNonEnglishLettersPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    value = value.replace(/[^\w\s]/gi, "");
    return value;
  }

}
