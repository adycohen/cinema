import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor(@Inject(HttpClient) private http:HttpClient) { }

  getMovie(title:string) {
    return this.http.get("https://www.omdbapi.com/?t=" + title + "&apikey=b9fc3cf3");
  }
  
}
