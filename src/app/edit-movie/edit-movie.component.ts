import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
import { ValidateTitle } from "../titleValidator";


@Component({
  selector: 'app-edit-movie',
  templateUrl: './edit-movie.component.html',
  styleUrls: ['./edit-movie.component.css']
})
export class EditMovieComponent implements OnInit {
  editForm: FormGroup;
  message:string;
  titles = [];

  constructor(private fb:FormBuilder,
    private dialogRef: MatDialogRef<EditMovieComponent>,
    @Inject(MAT_DIALOG_DATA) data) { 
      this.titles = data.existingTitles;

        this.editForm = fb.group({
        imdbID:new FormControl(data.id, [Validators.required]),
        Title:new FormControl(data.title, [Validators.required, ValidateTitle(this.titles)]),
        Year:new FormControl(data.year, [Validators.required, Validators.pattern("[0-9]{4}")]),
        Runtime:new FormControl(data.runtime, [Validators.required]),
        Genre:new FormControl(data.genre, [Validators.required]),
        Director:new FormControl(data.director, [Validators.required]) 
      });
    }

    
    @ViewChild('titleInput') titleInput: ElementRef; 
    ngOnInit() {
      setTimeout(() => this.titleInput.nativeElement.focus(), 400);
    }
    
  save() {
    if (this.editForm.valid == true) {
      this.dialogRef.close(this.editForm.value);
    }
    else {
      this.message = "Some fields are invalid!"
    }
  } 

  close() {
    this.dialogRef.close();
  }

}
