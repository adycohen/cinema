import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule, MatButtonModule, MatInputModule } from "@angular/material"; 
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';


import { AppComponent } from './app.component';
import { MoviesListComponent } from './movies-list/movies-list.component';
import { AddMovieComponent } from './add-movie/add-movie.component';
import { DeleteMovieComponent } from './delete-movie/delete-movie.component';
import { EditMovieComponent } from './edit-movie/edit-movie.component';

import { MoviesService } from './movies.service';
import { RemoveNonEnglishLettersPipe } from './remove-non-english-letters.pipe';


@NgModule({
  declarations: [
    AppComponent,
    MoviesListComponent,
    AddMovieComponent,
    DeleteMovieComponent,
    EditMovieComponent,
    RemoveNonEnglishLettersPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatDialogModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MatButtonModule,
    MatInputModule
  ],
  providers: [MoviesService],
  bootstrap: [AppComponent],
  entryComponents: [
    AddMovieComponent,
    DeleteMovieComponent,
    EditMovieComponent
  ]
})
export class AppModule { }
