import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogConfig } from "@angular/material";

import { AddMovieComponent } from '../add-movie/add-movie.component';
import { DeleteMovieComponent } from '../delete-movie/delete-movie.component';
import { EditMovieComponent } from '../edit-movie/edit-movie.component';

import { MoviesService } from '../movies.service';
import { element } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit {
  moviesList = [];
  titles = [
    "The Godfather",
    "Pulp Fiction",
    "Fight Club",
    "Forrest Gump",
    "Titanic",
    "The Pianist"
  ];

  constructor(@Inject(MoviesService) private moviesService:MoviesService,
  private dialog: MatDialog) { }

  ngOnInit() {
    this.titles.forEach(element => {    
      this.moviesService.getMovie(element)
      .subscribe(data => {
        this.moviesList.push(data);
      });
    });
  }

  onEditClick(index:number) {  
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      id: this.moviesList[index].imdbID,
      title: this.moviesList[index].Title,
      year: this.moviesList[index].Year,
      runtime: this.moviesList[index].Runtime,
      genre: this.moviesList[index].Genre,
      director: this.moviesList[index].Director,
      existingTitles: this.moviesList.map(a => a.Title)
    };

    const dialogRef = this.dialog.open(EditMovieComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => {
          if (data != null) {
            this.moviesList[index] = data;
          }
        });  
}

  onDeleteClick(index:number) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    const dialogRef = this.dialog.open(DeleteMovieComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data == true) { 
        this.moviesList.splice(index, 1);
        } 
      });
  }

  onAddClick() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.data = {
      existingTitles: this.moviesList.map(a => a.Title)
    };
    const dialogRef = this.dialog.open(AddMovieComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => { if (data != null) {
        this.moviesList.push(data);
       }});
  }

}
