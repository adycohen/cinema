import { Component, OnInit, Inject, ElementRef, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ValidateTitle } from "../titleValidator";

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {
  addForm:FormGroup;
  message:string;
  titles = [];

  constructor(private dialogRef: MatDialogRef<AddMovieComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
      this.titles = data.existingTitles;

    this.addForm = new FormGroup({
      imdbID:new FormControl(Math.floor(Math.random() * 1000) + 1,[Validators.required]),
      Title:new FormControl("",[Validators.required, ValidateTitle(this.titles)]),
      Year:new FormControl("",[Validators.required, Validators.pattern("[0-9]{4}")]),
      Runtime:new FormControl("",[Validators.required]),
      Genre:new FormControl("",[Validators.required]),
      Director:new FormControl("",[Validators.required]),
    });
   }
   
   @ViewChild('titleInput') titleInput: ElementRef; 
  ngOnInit() {
    setTimeout(() => this.titleInput.nativeElement.focus(), 400);
  }

  close() {
      this.dialogRef.close();
  }

  save() {
    if (this.addForm.valid == true) {
      this.dialogRef.close(this.addForm.value);
    }
    else {
      this.message = "Some fields are invalid!"
    }
  }

}
