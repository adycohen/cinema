import { AbstractControl, ValidatorFn } from '@angular/forms';

export function ValidateTitle (titles: string[]): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    var lowerCaseMovieTitle = control.value.toLowerCase();
    var lowerCaseTitles = [];
    for ( var i = 0; i < titles.length; i++)
    {
      lowerCaseTitles.push(titles[i].toLowerCase());
    }
    if (lowerCaseTitles.includes(lowerCaseMovieTitle)) {
      return { 'validTitle': true}
    }
      return null;  
    };
  }

 